<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function store(Request $request){

        // validation 
        $request->validate([
            'title' => ['required'],
            'description' => ['required'],
            'status' => ['required']
        ]);

        // store
        
        $recipe = new Recipe();
        $recipe->title = $request->title;
        $recipe->description = $request->description;
        $recipe->status = $request->status;

        $recipe->save();
    }
}
