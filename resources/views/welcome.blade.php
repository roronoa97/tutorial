<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recipes</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
</head>
<body class="bg-light">
    <div class="container">

        <h1>Recipes Form</h1>
    
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('recipes.post') }}">
                    @csrf
                    <div class="form-group">
                      <label>Title</label>
                      <input type="text" class="form-control @error('title') is-invalid @enderror" name="title">
                      @error('title')
                        <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label>description</label>
                        <textarea id="" cols="30" rows="10" class="form-control" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="status" class="custom-control-input" value="active">
                            <label class="custom-control-label" for="customRadio1">Active</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="status" class="custom-control-input" value="inactive">
                            <label class="custom-control-label" for="customRadio2">Inactive</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>